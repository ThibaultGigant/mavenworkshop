package fr.openvalue.devops.workshop.maven

package object configuration {

  /** case class used to parse and store each message element from the conf file */
  case class Message(
                      mKey: String,
                      mMessage: String,
                      mDoEncrypt: Boolean = true
                    )

}
