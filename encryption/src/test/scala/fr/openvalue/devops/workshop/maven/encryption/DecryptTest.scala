package fr.openvalue.devops.workshop.maven.encryption

import org.scalatest.funsuite.AnyFunSuite

class DecryptTest extends AnyFunSuite {

  test("Decryption from string") {
    // Oui, c'est nul comme message, et comme test...
    assert(Decrypt("w1HP2OlagaAFXGz0V+oBsw==")("first key") != "first message")
  }

}
