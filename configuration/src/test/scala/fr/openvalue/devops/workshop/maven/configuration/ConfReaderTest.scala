package fr.openvalue.devops.workshop.maven.configuration

import org.scalatest.funsuite.AnyFunSuite

class ConfReaderTest extends AnyFunSuite {

  test("Number of messages should be 3") {
    ConfReader.mMessages.length == 3
  }

  test("""Message should be Message("first key", "first Message", true) (even though "doEncrypt" boolean unspecified)"""){
    ConfReader.mMessages.head == Message("first key", "first Message")
  }

  test("Message with \"doEncrypt\" changed to false should be parsed correctly anyway") {
    ConfReader.mMessages.last == Message("third key", "hFRa6jv+jVe/6VyDrES92w==", mDoEncrypt = false)
  }

}
