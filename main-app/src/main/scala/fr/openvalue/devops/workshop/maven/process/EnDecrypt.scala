package fr.openvalue.devops.workshop.maven.process

import fr.openvalue.devops.workshop.maven.configuration.Message
import fr.openvalue.devops.workshop.maven.encryption.{Decrypt, Encrypt}

/** Encrypt or decrypt a message */
object EnDecrypt {

  def apply(iMessage: String, iDoEncrypt: Boolean)(implicit iKey: String): String = {
    if (iDoEncrypt) {
      Encrypt(iMessage)
    }
    else {
      Decrypt(iMessage)
    }
  }

  def apply(iMessage: Message): String = {
    EnDecrypt(iMessage.mMessage, iMessage.mDoEncrypt)(iMessage.mKey)
  }

}
