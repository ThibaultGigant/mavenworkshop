package fr.openvalue.devops.workshop.maven.encryption

import org.apache.commons.codec.binary.Base64

import javax.crypto.Cipher

/** Used to encrypt a message with the given key */
object Encrypt extends Encryption(Cipher.ENCRYPT_MODE) {

  def apply(iMessage: String)(implicit iKey: String): String = {
    Base64.encodeBase64String(getCipher.doFinal(iMessage.getBytes("UTF-8")))
  }

}
