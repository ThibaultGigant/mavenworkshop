package fr.openvalue.devops.workshop.maven.encryption

import java.security.MessageDigest
import java.util
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

/** Common trait to encrypt or decrypt messages */
trait Encryption(iCipherMode: Int) {

  private val mSALT: String = "lZ0THJ2kz09#HwWAuD6Zm#$!PRzea0OTIDGp3Kewxew2kUaD$8l"

  /** Transform (encrypt or decrypt) the message, using the provided key */
  def apply(iMessage: String)(implicit iKey: String): String

  /** Generate the spec used to generate a cipher object */
  private def keyToSpec(implicit iKey: String): SecretKeySpec = {
    var lKeyBytes: Array[Byte] = (mSALT + iKey).getBytes("UTF-8")
    val lSha: MessageDigest = MessageDigest.getInstance("SHA-1")
    lKeyBytes = lSha.digest(lKeyBytes)
    lKeyBytes = util.Arrays.copyOf(lKeyBytes, 32)
    new SecretKeySpec(lKeyBytes, "AES")
  }

  /** Use this tool to cipher or decipher a message */
  protected def getCipher(implicit iKey: String): Cipher = {
    val cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING")
    cipher.init(iCipherMode, keyToSpec(iKey))
    cipher
  }

}
