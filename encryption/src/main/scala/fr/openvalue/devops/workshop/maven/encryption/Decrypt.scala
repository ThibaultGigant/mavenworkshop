package fr.openvalue.devops.workshop.maven.encryption

import org.apache.commons.codec.binary.Base64

import javax.crypto.Cipher

/** Used to decrypt a message encrypted by using the given key */
object Decrypt extends Encryption(Cipher.DECRYPT_MODE) {

  def apply(iMessage: String)(implicit iKey: String): String = {
    new String(getCipher(iKey).doFinal(Base64.decodeBase64(iMessage)))
  }

}
