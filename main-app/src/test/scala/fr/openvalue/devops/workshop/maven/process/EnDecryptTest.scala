package fr.openvalue.devops.workshop.maven.process

import org.scalatest.funsuite.AnyFunSuite

import fr.openvalue.devops.workshop.maven.configuration.Message
import fr.openvalue.devops.workshop.maven.encryption.{Decrypt, Encrypt}

class EnDecryptTest extends AnyFunSuite {

  test("Encrypting string") {
    assert(EnDecrypt("first message", true)("first key") == "w1HP2OlagaAFXGz0V+oBsw==")
  }

  test("Encrypting Message object") {
    assert(EnDecrypt(Message("first key", "first message")) == "w1HP2OlagaAFXGz0V+oBsw==")
  }

  test("Decryption from string") {
    assert(EnDecrypt("w1HP2OlagaAFXGz0V+oBsw==", false)("first key") == "first message")
  }

  test("Decrypting from Message object") {
    assert(EnDecrypt(Message("first key", "w1HP2OlagaAFXGz0V+oBsw==", false)) == "first message")
  }

}
