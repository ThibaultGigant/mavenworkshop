package fr.openvalue.devops.workshop.maven

import fr.openvalue.devops.workshop.maven.configuration.ConfReader
import fr.openvalue.devops.workshop.maven.process.EnDecrypt

/** This is the main function, that will be launched */
object App {

  def main(iArgs: Array[String]): Unit = {

    ConfReader.mMessages.map(EnDecrypt(_)).foreach(println)

  }

}
