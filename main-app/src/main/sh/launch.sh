#!/bin/bash

ROOT_PATH="$( cd "$(dirname "$0")" ; pwd -P )/../"
BIN_PATH="$ROOT_PATH/bin/"
MAIN_APP_NAME="${project.artifactId}-${project.version}-jar-with-dependencies.jar"
MAIN_APP_PATH="$BIN_PATH/$MAIN_APP_NAME"
CONF_PATH="$ROOT_PATH/conf/"
CONF_FILE="app.json"

if [[ -n $(jar tf "$MAIN_APP_PATH" | grep "$CONF_FILE") ]]
then
  zip -d "$MAIN_APP_PATH" "$CONF_FILE" > /dev/null
fi

jar uf "$BIN_PATH/$MAIN_APP_NAME" -C "$CONF_PATH" "$CONF_FILE"

CLASSPATH=$MAIN_APP_PATH
java -cp "$CLASSPATH" fr.openvalue.devops.workshop.maven.App
