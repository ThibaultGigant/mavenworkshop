# Workshop maven

Ce projet a juste pour objectif de prendre en main les bases de maven.
Pour ce faire, un petit exercice a été créé avec pour finalité la génération un package de livrables fonctionnel,
permettant de déchiffrer un message.

<!-- TOC -->
* [Workshop maven](#workshop-maven)
  * [Prérequis](#prérequis)
  * [Comprendre les sources](#comprendre-les-sources)
  * [Quoi faire...](#quoi-faire)
    * [La compilation](#la-compilation)
    * [Les phases](#les-phases)
    * [Le packaging](#le-packaging)
    * [Les dépendances](#les-dépendances)
<!-- TOC -->

## Prérequis

L'idéal serait d'avoir IntelliJ IDEA installé.
Lors de l'installation, ou dans les paramètres, vous pouvez demander IntelliJ d'installer Java 8+ et Maven 3.5+.
Voici le lien pour installation, quel que soit l'OS :
[https://www.jetbrains.com/fr-fr/idea/download/](https://www.jetbrains.com/fr-fr/idea/download/).

***_Attention !_*** Il est inutile pour ce workshop d'installer la version "Ultimate". La version "Community" suffit amplement.

A minima, il est nécessaire d'avoir les outils suivants installés :

- `java` : selon votre OS, vous pouvez par exemple suivre les instructions depuis [https://openjdk.org/](https://openjdk.org/)
- `maven` : idem, vous pouvez suivre les instructions du site officiel [https://maven.apache.org/](https://maven.apache.org/)

Cet outil est paramétré pour s'exécuter sur Unix.
Sur Windows, de nombreuses options vous sont proposées pour lancer du shell et exécuter des commandes POSIX.
Nous vous recommandons d'installer Linux sur Windows avec WSL : [https://learn.microsoft.com/fr-fr/windows/wsl/install](https://learn.microsoft.com/fr-fr/windows/wsl/install)

## Comprendre les sources

Ce projet est constitué de plusieurs modules rédigés en `scala`, qui vont travailler de concert afin de chiffrer et/ou déchiffrer des "messages".
Ceux-ci sont listés dans un fichier de configuration, fourni au programme lors de son exécution.
Dans notre projet, il est [ici](main-app/src/main/conf/app.json).

Il est lu par le module [`configuration`](configuration/pom.xml) qui a pour seul et unique responsabilité de parser ce fichier.
Il en extrait une séquence d'objets [`Message`](configuration/src/main/scala/fr/openvalue/devops/workshop/maven/configuration/package.scala).

Le module [`main-app`](main-app/pom.xml) est le module "principal". C'est lui qui sera exécuté.
Il dépend donc du module `configuration` pour la raison déjà évoquée.
De plus, comme c'est l'objet de ce projet, il a besoin du module [`encryption`](encryption/pom.xml) pour chiffrer et/ou déchiffrer les messages.

```mermaid
---
title: Architecture globale
---
flowchart TB
    id[main-app]
    configuration --> id
    encryption --> id
```

## Quoi faire...

### La compilation
Commencez par survoler les fichiers et comprendre qu'il y a, malheureusement, du rouge partout...

![no-scala-sdk](images/no-scala-sdk.png)

D'après IntelliJ, il manque déjà un SDK pour Scala.
Cela veut dire qu'IntelliJ n'est pas prêt à lire ni compiler ce code MAGNIFIQUE !

Commençons donc par ajouter le plugin qui nous permet de compiler ce langage. Le voici :

```xml
      <plugin>
        <groupId>net.alchim31.maven</groupId>
        <artifactId>scala-maven-plugin</artifactId>
        <version>${scala-maven-plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>compile</goal>
              <goal>testCompile</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <scalaVersion>${scala.version}</scalaVersion>
        </configuration>
      </plugin>
```

Il faut l'ajouter à un `pom.xml`, mais lequel ? ou lesquels ? Dans quelle section ?

*Hint :* Tous les modules sont en scala, et auront donc besoin de compiler dans ce langage.

---------

### Les phases

Le SDK est maintenant connu, et on peut essayer de compiler avec un petit `mvn clean package` pour commencer.
On pourrait lancer `mvn clean install`, mais il n'est pas nécessaire de pourrir votre local repo
(qui stocke les librairies nécessaires à vos projets, et les projets que vous "installez").
Je doute juste que vous ayez un jour besoin à nouveau de ce projet... Mais c'est vous qui voyez.

On indique alors à *Maven* d'effectuer la phase `clean`, afin de supprimer un éventuel dossier `target`
et tout ce qu'il contient, s'il existe déjà.
De plus, on souhaite par la suite qu'il effectue la phase `package` afin qu'il "rassemble" les sources compilées
dans un format intéressant, tel qu'un `jar`.

Maintenant ce sont les tests qui ne passent pas ! Dommage, étudions ce qui se passe :
On voit notamment comme message d'erreur :

```text
DecryptTest:
- Decryption from string *** FAILED ***
  "first message" equaled "first message" (DecryptTest.scala:9)
EncryptTest:
- Encrypting string *** FAILED ***
  "w1HP2OlagaAFXGz0V+oBsw==" equaled "w1HP2OlagaAFXGz0V+oBsw==" (EncryptTest.scala:9)
```

Les erreurs sont donc dans ces deux fichiers :
[DecryptTest](encryption/src/test/scala/fr/openvalue/devops/workshop/maven/encryption/DecryptTest.scala) et
[EncrypTest](encryption/src/test/scala/fr/openvalue/devops/workshop/maven/encryption/EncryptTest.scala).
Je me suis évidemment trompé (volontairement), ce sont bien des tests d'égalité, pas d'inégalité !

Corrigez cela, et réfléchissons un peu... Nous avons demandé l'exécution de deux `phases` : `clean` et `package`.
Pourquoi l'exécution a-t-elle planté dans une phase de tests, non demandée ?

Les phases sont exécutées séquentiellement, dans l'ordre,
sauf s'il n'y a eu aucune modification depuis leur dernière éxécution réussie.
Il y a donc eu l'exécution des phases suivantes :

1. `validate` : vérification que le projet est "valide", et contient toutes les informations nécessaires
2. `compile` : compilation des sources
3. `test` : exécution des tests... et c'est là que ça a raté. Nous n'avons donc pas pu atteindre la phase suivante...
4. `package` : rassembler les sources compilées dans un format intéressant tel qu'un `jar`

Et si on avait voulu, on aurait pu demander les phases suivantes :

5. `verify` : effectuer certains derniers checks, tests d'intégration, et de la qualité du livrable
6. `install` : installe (copie...) le package créé et vérifié dans le *local repository*, pour pouvoir être utilisé par d'autres projets (en local)
7. `deploy` : copier le package final dans le *remote repository*

Source : [ici](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)

---------

### Le packaging

Générons maintenant nos livrables. Vous vous en doutez, vous pourriez le faire à la main.
Mais ce serait quand même assez laborieux.

Heureusement, de nombreux outils existent, et vous pouvez les utiliser en ajoutant simplement des plugins.
L'un d'eux permet de générer un livrable : `maven-assembly-plugin`.
Mais ne nous contentons pas de si peu.
Lorsque vous aurez un "vrai" projet, vous aurez plusieurs environnements sur lesquels vous travaillerez.
Si vous êtes chez des gens bien, vous aurez *au moins* les environnements suivants :

- développement
- intégration
- préproduction
- production

Tentons de générer les livrables en ajoutant ce plugin.
Il n'est pas nécessaire de le faire partout, ni même dans le pom parent.
En effet, un seul module sera réellement lancé : `main-app`.
Vous pouvez donc rajouter ce plugin dans le pom de celui-ci uniquement.

```xml
<build>

        <plugins>

            <plugin>
                <groupId>com.soebes.maven.plugins</groupId>
                <artifactId>iterator-maven-plugin</artifactId>
                <version>${iterator-mvn-plugin.version}</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>iterator</goal>
                        </goals>
                        <configuration>
                            <items>
                                <item>dev</item>
                                <item>int</item>
                                <item>preprod</item>
                                <item>prd</item>
                            </items>
                            <pluginExecutors>
                                <pluginExecutor>
                                    <plugin>
                                        <groupId>org.apache.maven.plugins</groupId>
                                        <artifactId>maven-assembly-plugin</artifactId>
                                        <version>${mvn-assembly-plugin.version}</version>
                                    </plugin>
                                    <goal>single</goal>
                                    <configuration>
                                        <filters>
                                            <!--suppress UnresolvedMavenProperty -->
                                            <filter>${project.basedir}/src/main/assembly/filters/${item}.properties</filter>
                                        </filters>
                                        <finalName>${project.artifactId}</finalName>
                                        <descriptors>
                                            <descriptor>${project.basedir}/src/main/assembly/assembly.xml</descriptor>
                                        </descriptors>
                                    </configuration>
                                </pluginExecutor>
                            </pluginExecutors>
                        </configuration>
                    </execution>
                </executions>

            </plugin>

        </plugins>

    </build>
```

Il indique qu'il va falloir exécuter le plugin `maven-assembly-plugin` plusieurs fois. Une fois pour chaque item :
`dev`, `int`, `preprod`, et `prod`. Donc une fois pour chaque environnement de destination.

À chaque fois, il utilisera le fichier [assembly.xml](main-app/src/main/assembly/assembly.xml).
Ce qui est intéressant dans ce fichier est la liste de `fileSets`. Chaque `fileSet` ressemble à :

```xml
<fileSet>
    <directory>{{ dossier où récupérer les fichiers }}</directory>
    <outputDirectory>{{ dossier de destination, où iront les fichiers sélectionnés }}</outputDirectory>
    <includes>
        <include>{{ filtre à appliquer dans la récupération des fichiers }}</include>
    </includes>
</fileSet>
```

Si de plus, il contient une balise `filtered` avec la valeur `true`,
il remplacera les variables définies dans le fichier correspondant à l'environnement.
Par exemple, pour l'item `dev`, il utilisera le fichier [dev.properties](main-app/src/main/assembly/filters/dev.properties).

Après un nouveau `mvn clean package`, dans le dossier `target` du module en question,
vous trouverez alors ENFIN nos livrables, avec un fichier "zip" pour chaque environnement.

-----------

### Les dépendances

Prenez un des packages (zip) générés, et dézippez-le. Vous trouverez qu'il manque un dossier important : `bin`.
C'est quelque peu embêtant, étant donné que c'est justement l'exécutable que nous tentons de générer, pour le lancer.

Pourquoi cela n'a-t-il pas fonctionné ? Alors que le fichier de conf et le script shell ont bien été copiés...

En regardant de plus près le fichier [assembly.xml](main-app/src/main/assembly/assembly.xml), on comprend mieux.
Ce fichier est celui qui dicte au plugin ce qu'il doit faire dans le package.

Concrètement, ce qui est important est la balise `<fileSets>` qui contient l'ensemble des `<fileSet>` à traiter.
Un `fileSet` est un ensemble de fichiers qui est à récupérer à un endroit, pour les placer dans un dossier du livrable.
Par exemple, dans le fichier, on peut trouver :

```xml
<fileSet>
  <directory>${project.build.directory}</directory>
  <outputDirectory>/bin</outputDirectory>
  <includes>
    <include>*-jar-with-dependencies.jar</include>
  </includes>
</fileSet>
```

Cela signifie que l'on va :

1. récupérer des fichiers qui se trouvent dans le dossier résultant du build
(`${project.build.directory}`, correspondant au dossier `target` créé)
2. pour les copier dans le dossier de destination `/bin`
(ici, la racine est considérée comme étant le dossier racine du livrable)
3. en n'incluant dans cette copie que les fichiers qui correspondent au pattern `*-jar-with-dependencies.jar`

Mais lorsqu'on regarde le dossier `target` en question, on ne voit qu'un fichier `jar`.
Celui-ci ne correspond pas au pattern indiqué ! 

On comprend donc que le problème provient du fait qu'on ne crée que des jars "simples" (ou "skinny").
C'est-à-dire qu'ils ne contiennent aucune dépendance.
Il y a plusieurs types de jars : skinny, thin, hollow, et fat (ou uber).
Pour en apprendre plus, vous pouvez lire [cet article](https://dzone.com/articles/the-skinny-on-fat-thin-hollow-and-uber).

Nous allons donc remédier à cela, et créer un jar avec des dépendances, en ajoutant ce plugin :

```xml

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-assembly-plugin</artifactId>
        <version>${mvn-assembly-plugin.version}</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>single</goal>
            </goals>
            <configuration>
              <archive>
                <manifest>
                  <mainClass>${main.class}</mainClass>
                </manifest>
              </archive>
              <descriptorRefs>
                <descriptorRef>jar-with-dependencies</descriptorRef>
              </descriptorRefs>
            </configuration>
          </execution>
        </executions>
      </plugin>

```

Celui-ci indique qu'on veut générer un jar avec les dépendances, qu'on distinguera par le suffixe "jar-with-dependencies".

Encore une fois : Où doit-on le placer ? dans quel(s) module(s) ? Ou carrément dans le pom parent ?

N'oubliez pas que les plugins s'exécutent DANS L'ORDRE.
Il faut donc que le jar avec dépendances soit présent AVANT l'exécution du plugin qui récupère ce fichier pour le copier dans le livrable final.

---------

Une fois la réponse trouvée, et le plugin ajouté, rejouons un `maven clean package`.

Cette fois, on a bien des jars dans un dossier "bin", en plus du fichier de conf et du script de lancement.
Essayons de le lancer :

```shell
bash /path/to/launch.sh
```

Il se peut que vous deviez ajouter les droits d'exécution à ce fichier :

```shell
chmod u+x /path/to/launch.sh
```

Zut, encore une erreur, alors que la compilation et les tests sont passés !

```text
Exception in thread "main" java.lang.NoClassDefFoundError: scala/Function1
	at fr.openvalue.devops.workshop.maven.App.main(App.scala)
Caused by: java.lang.ClassNotFoundException: scala.Function1
	at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:641)
	at java.base/jdk.internal.loader.ClassLoaders$AppClassLoader.loadClass(ClassLoaders.java:188)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:521)
	... 1 more
```

Que comprenez-vous de cette erreur ? Il n'arrive apparemment pas à charger des fonctions du langage `scala` lui-même...

Pourtant, notre module `main-app` a comme dépendance le module `encryption`,
qui lui-même contient une dépendance au langage scala.
Quel est donc le problème ?

Le scope de la dépendance au langage scala est à `provided`.
Cela signifie que l'on considère qu'à l'exécution, celle-ci est nécessaire,
mais qu'on ne l'ajoute pas au "fat jar" lors du packaging.
Il faut donc changer le scope de la dépendance dans le module `encryption` pour que ce soit *compile*.
Pour ce faire, il suffit de retirer la balise `<scope>` pour que la valeur par défaut soit prise,
ou changer directement sa valeur pour être sûr de ce qu'on fait.

Plusieurs choix s'offrent alors à vous, des choix de bourrins et d'autres plus subtils :

1. Le plus simple : Laisser comme tel.
On pourrait rétorquer que le jour où notre application n'utiliserait plus ce module, mais un autre, pour chiffrer/déchiffrer,
on se retrouverait dans la panade !
2. Bouger la dépendance dans le pom parent. Ainsi, quel que soit le module "maître",
on aurait toujours accès à cette librairie. Mais dans ce cas, on se retrouverait alors plusieurs fois avec la dépendance
lors de l'exécution. En effet, tous les jars avec dépendance se retrouveraient avec celle-ci incluse. BOURRIN !
Je me demande même si ça pourrait ne pas poser problème pour ambigüité de classe dans le classpath...
3. Bouger la dépendance dans le pom du module "maître" (main-app). On sait où se situe le besoin.
On place la dépendance où c'est nécessaire.
Mais ainsi, on n'indique pas que les autres modules nécessitent cette dépendance (au moins qu'elle soit fournie au runtime).
4. Copier la dépendance en scope provided dans tous les modules, SAUF dans le module maître où ce scope devra être en compile.
De cette manière, on signifie à d'éventuels utilisateurs de ces modules que cette dépendance est nécessaire,
mais qu'il faut la fournir pour une bonne exécution !

Faites votre choix.

Problème réglé ? Un petit `mvn clean package` et vous pourrez faire fonctionner correctement ce petit code !

Quel est le message caché ?

-----------

**Bonne chance !**
