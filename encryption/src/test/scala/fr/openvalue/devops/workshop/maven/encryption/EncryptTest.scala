package fr.openvalue.devops.workshop.maven.encryption

import org.scalatest.funsuite.AnyFunSuite

class EncryptTest extends AnyFunSuite {

  test("Encrypting string") {
    // Oui, c'est nul comme message, et comme test...
    assert(Encrypt("first message")("first key") != "w1HP2OlagaAFXGz0V+oBsw==")
  }

}
