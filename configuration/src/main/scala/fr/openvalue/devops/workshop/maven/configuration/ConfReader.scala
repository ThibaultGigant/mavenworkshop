package fr.openvalue.devops.workshop.maven.configuration

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ValueReader

object ConfReader {

  /** Access to the entire conf file */
  private val mAppConf: Config = ConfigFactory.load("app.json")

  /** Implicit object used to parse each message element */
  private implicit val mMessageReader: ValueReader[Message] = ValueReader.relative { msg =>
    Message(
      mKey = msg.getString("key"),
      mMessage = msg.getString("message"),
      mDoEncrypt = msg.as[Option[Boolean]]("doEncrypt").getOrElse(true)
    )
  }

  /** List of messages parsed from the conf file */
  val mMessages: Seq[Message] = mAppConf.as[Seq[Message]]("messages")

}
